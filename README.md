# README #

Requirements to execute test
 - maven
 - java8
 
# project structure #
cucumber features with tests are located in:
- src/test/resources/features

project properties are located in: 
- src/test/resources/config/application.properties

description of the pages are located in: 
- src/main/java/com/template/pages

# how to run #
if you execute tests not at windows machine, then need to change path to chrome driver into application.properties to one of them:
- src/test/resources/webdrivers/linux64/chromedriver - for linux 64
- src/test/resources/webdrivers/linux32/chromedriver - for linux 32
- src/test/resources/webdrivers/mac/chromedriver - for mac

<b>And execute command to give drivers rights to execute: chmod -R 777 src/test/resources/webdrivers/ </b> 

For run test you need to execute command: 
- mvn clean install -DTAGS=tags

where tags name of test or group of tests.
RT - tag to execute all tests. 
Tags are located at features file for each test.

# generate test report #
After test passed, to generate allure test report you need to execute command at project root directory:
- mvn site:site

after that, will be generate report at folder: 
- target/site/allure-maven-plugin.html
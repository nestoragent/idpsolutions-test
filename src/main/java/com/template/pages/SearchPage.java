package com.template.pages;

import com.template.lib.Page;
import com.template.lib.PageFactory;
import com.template.lib.annotations.ElementTitle;
import com.template.lib.annotations.PageEntry;
import com.template.lib.extensions.DriverExtension;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

import java.util.List;

/**
 * Created by nestor on 12.07.2017.
 */
@PageEntry(title = "Search")
public class SearchPage extends Page {

    @FindBy(id = "q")
    @ElementTitle("input search")
    public WebElement inputSearch;

    @FindBy(css = "input.search-button")
    @ElementTitle("submit search")
    public WebElement submitSearch;

    public SearchPage() {
        PageFactory.initElements(
                new HtmlElementDecorator(new HtmlElementLocatorFactory(PageFactory.getDriver())), this);
        new WebDriverWait(PageFactory.getDriver(), PageFactory.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(inputSearch));
    }

    public void search_item(String item) {
        fillField(inputSearch, item);
        clickWebElement(submitSearch);
        DriverExtension.waitForPageToLoad();
    }
}

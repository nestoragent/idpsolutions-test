package com.template.pages;

import com.template.lib.Page;
import com.template.lib.PageFactory;
import com.template.lib.annotations.ElementTitle;
import com.template.lib.annotations.PageEntry;
import com.template.lib.extensions.DriverExtension;
import cucumber.api.DataTable;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

import java.util.List;

/**
 * Created by nestor on 12.07.2017.
 */
@PageEntry(title = "Search result")
public class SearchResultsPage extends Page {

    @FindBy(css = "div.search-results")
    @ElementTitle("init elem")
    public WebElement initElem;

    public SearchResultsPage() {
        PageFactory.initElements(
                new HtmlElementDecorator(new HtmlElementLocatorFactory(PageFactory.getDriver())), this);
        new WebDriverWait(PageFactory.getDriver(), PageFactory.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(initElem));
    }

    public void open_item(String number) {
        new WebDriverWait(PageFactory.getDriver(), PageFactory.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div.search-results div.product-item div.picture")));
        List<WebElement> searchResult = PageFactory.getDriver().findElements(By.cssSelector("div.search-results div.product-item div.picture"));
        Assert.assertFalse("Didn't found any results.", searchResult.isEmpty());
        int position = Integer.parseInt(number);
        clickWebElement(searchResult.get(position - 1));
        DriverExtension.waitForPageToLoad();
    }

}

package com.template.pages;

import com.template.lib.Page;
import com.template.lib.PageFactory;
import com.template.lib.annotations.ElementTitle;
import com.template.lib.annotations.PageEntry;
import com.template.lib.extensions.DriverExtension;
import cucumber.api.DataTable;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

import java.util.*;

/**
 * Created by nestor on 12.07.2017.
 */
@PageEntry(title = "Product description")
public class ProductDescriptionPage extends Page {

    @FindBy(css = "div.product-name h1")
    @ElementTitle("product name")
    public WebElement divProductName;

    @FindBy(css = "div.short-description")
    @ElementTitle("short description")
    public WebElement divShortDescription;

    @FindBy(css = "div.full-description")
    @ElementTitle("full description")
    public WebElement divFullDescription;

    @FindBy(css = "span[id*='sku']")
    @ElementTitle("SKU")
    public WebElement divSKU;

    @FindBy(css = "span.price-value-1")
    @ElementTitle("span price")
    public WebElement spanPrice;

    public ProductDescriptionPage() {
        PageFactory.initElements(
                new HtmlElementDecorator(new HtmlElementLocatorFactory(PageFactory.getDriver())), this);
        new WebDriverWait(PageFactory.getDriver(), PageFactory.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(divProductName));
    }

    public void check_search_result(DataTable dataTable) {
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        if (null != data.get("productName") && !data.get("productName").equals(""))
            Assert.assertEquals(divProductName.getText().toLowerCase().trim(), data.get("productName").toLowerCase().trim());
        if (null != data.get("shortDescription") && !data.get("shortDescription").equals(""))
            Assert.assertEquals(divShortDescription.getText().toLowerCase().trim(), data.get("shortDescription").toLowerCase().trim());

        if (null != data.get("fullDescription") && !data.get("fullDescription").equals(""))
            Assert.assertTrue(divFullDescription.getText().toLowerCase().replaceAll("\n", " ").trim().contains(data.get("fullDescription").toLowerCase().trim()));
        if (null != data.get("SKU") && !data.get("SKU").equals(""))
            Assert.assertEquals(divSKU.getText().toLowerCase().trim(), data.get("SKU").toLowerCase().trim());
        if (null != data.get("content") && !data.get("content").equals(""))
            Assert.assertTrue(spanPrice.getAttribute("content").toLowerCase().contains(data.get("price").toLowerCase().trim()));
        //check tags
        if (null != data.get("tags") && !data.get("tags").equals("")) {
            List<WebElement> tagsWeb = PageFactory.getDriver().findElements(By.cssSelector("div.product-tags-list li.tag a"));
            ArrayList<String> tagsText = new ArrayList<>();
            tagsWeb.forEach(web -> tagsText.add(web.getText()));
            for (String tag : data.get("tags").split(";")) {
                Assert.assertTrue(tagsText.contains(tag));
            }
        }
    }

}

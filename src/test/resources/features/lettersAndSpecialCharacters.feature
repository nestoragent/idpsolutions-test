Feature: Search GTIN containing letters and special characters

  @RT
  @UC-4
  Scenario: Search GTIN containing letters and special characters from main page
    * user go to the idpsolutions
    * user is on the page "Main"
    * user (search item) "123!"
    * user is on the page "Search result"
    * user (open item) "1"
    * user is on the page "Product description"
    * user (check_search_result) data
      | productName      | Beats Pill 2.0 Wireless Speaker                                                                                                                                                                                                                                                  |
      | shortDescription | Pill 2.0 Portable Bluetooth Speaker (1-Piece): Watch your favorite movies and listen to music with striking sound quality. This lightweight, portable speaker is easy to take with you as you travel to any destination, keeping you entertained wherever you are.           |
      | fullDescription  | Pair and play with your Bluetooth® device with 30 foot range Built-in speakerphone 7 hour rechargeable battery Power your other devices with USB charge out Tap two Beats Pills™ together for twice the sound with Beats Bond |
      | SKU              | BP_20_WSP                                                                                                                                                                                                                                                                        |
      | price            | 79,99                                                                                                                                                                                                                                                                            |
      | tags             | computer;cool                                                                                                                                                                                                                                                                    |

  @RT
  @UC-4-search
  Scenario: Search GTIN containing letters and special characters from search page
    * user go to the idpsolutions
    * user is on the page "Main"
    * user (open_search_page)
    * user is on the page "Search"
    * user (search item) "123!"
    * user is on the page "Search result"
    * user (open item) "1"
    * user is on the page "Product description"
    * user (check_search_result) data
      | productName      | Beats Pill 2.0 Wireless Speaker                                                                                                                                                                                                                                                  |
      | shortDescription | Pill 2.0 Portable Bluetooth Speaker (1-Piece): Watch your favorite movies and listen to music with striking sound quality. This lightweight, portable speaker is easy to take with you as you travel to any destination, keeping you entertained wherever you are.           |
      | fullDescription  | Pair and play with your Bluetooth® device with 30 foot range Built-in speakerphone 7 hour rechargeable battery Power your other devices with USB charge out Tap two Beats Pills™ together for twice the sound with Beats Bond |
      | SKU              | BP_20_WSP                                                                                                                                                                                                                                                                        |
      | price            | 79,99                                                                                                                                                                                                                                                                            |
      | tags             | computer;cool                                                                                                                                                                                                                                                                    |

Feature: User should be able to search for standard nopCommerce search fields

  @RT
  @UC-3
  Scenario: User should be able to search for standard nopCommerce search fields from main page
    * user go to the idpsolutions
    * user is on the page "Main"
    * user (search item) "abcdefgi"
    * user is on the page "Search result"
    * user (open item) "1"
    * user is on the page "Product description"
    * user (check_search_result) data
      | productName      | Apple iCam                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
      | shortDescription | Photography becomes smart                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
      | fullDescription  | A few months ago we featured the amazing WVIL camera, by many considered the future of digital photography. This is another very good looking concept, iCam is the vision of Italian designer Antonio DeRosa, the idea is to have a device that attaches to the iPhone 5, which then allows the user to have a camera with interchangeable lenses. The device would also feature a front-touch screen and a projector. Would be great if apple picked up on this and made it reality. |
      | SKU              | APPLE_CAM                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
      | price            | 1300                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

  @RT
  @UC-4-search
  Scenario: User should be able to search for standard nopCommerce search fields from search page
    * user go to the idpsolutions
    * user is on the page "Main"
    * user (open_search_page)
    * user is on the page "Search"
    * user (search item) "abcdefgi"
    * user is on the page "Search result"
    * user (open item) "1"
    * user is on the page "Product description"
    * user (check_search_result) data
      | productName      | Apple iCam                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
      | shortDescription | Photography becomes smart                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
      | fullDescription  | A few months ago we featured the amazing WVIL camera, by many considered the future of digital photography. This is another very good looking concept, iCam is the vision of Italian designer Antonio DeRosa, the idea is to have a device that attaches to the iPhone 5, which then allows the user to have a camera with interchangeable lenses. The device would also feature a front-touch screen and a projector. Would be great if apple picked up on this and made it reality. |
      | SKU              | APPLE_CAM                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
      | price            | 1300                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

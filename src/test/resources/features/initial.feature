Feature: Search the products using GTIN

  @RT
  @UC-1
  Scenario: User should be able to search for the products using GTIN from main page
    * user go to the idpsolutions
    * user is on the page "Main"
    * user (search item) "123"
    * user is on the page "Search result"
    * user (open item) "1"
    * user is on the page "Product description"
    * user (check_search_result) data
      | productName      | Build your own computer                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
      | shortDescription | Build it                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
      | fullDescription  | Fight back against cluttered workspaces with the stylish IBM zBC12 All-in-One desktop PC, featuring powerful computing resources and a stunning 20.1-inch widescreen display with stunning XBRITE-HiColor LCD technology. The black IBM zBC12 has a built-in microphone and MOTION EYE camera with face-tracking technology that allows for easy communication with friends and family. And it has a built-in DVD burner and Sony's Movie Store software so you can create a digital entertainment library for personal viewing at your convenience. Easy to setup and even easier to use, this JS-series All-in-One includes an elegantly designed keyboard and a USB mouse. |
      | SKU              | COMP_CUST                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
      | price            | 1200                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
      | tags             | computer;awesome                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |

  @RT
  @UC-1-search
  Scenario: User should be able to search for the products using GTIN from seach page
    * user go to the idpsolutions
    * user is on the page "Main"
    * user (open_search_page)
    * user is on the page "Search"
    * user (search item) "123"
    * user is on the page "Search result"
    * user (open item) "1"
    * user is on the page "Product description"
    * user (check_search_result) data
      | productName      | Build your own computer                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
      | shortDescription | Build it                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
      | fullDescription  | Fight back against cluttered workspaces with the stylish IBM zBC12 All-in-One desktop PC, featuring powerful computing resources and a stunning 20.1-inch widescreen display with stunning XBRITE-HiColor LCD technology. The black IBM zBC12 has a built-in microphone and MOTION EYE camera with face-tracking technology that allows for easy communication with friends and family. And it has a built-in DVD burner and Sony's Movie Store software so you can create a digital entertainment library for personal viewing at your convenience. Easy to setup and even easier to use, this JS-series All-in-One includes an elegantly designed keyboard and a USB mouse. |
      | SKU              | COMP_CUST                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
      | price            | 1200                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
      | tags             | computer;awesome                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |

  @RT
  @UC-2
  Scenario: User should be able to search for the products using GTIN from main page
    * user go to the idpsolutions
    * user is on the page "Main"
    * user (search item) "12345"
    * user is on the page "Search result"
    * user (open item) "1"
    * user is on the page "Product description"
    * user (check_search_result) data
      | productName     | Lenovo IdeaCentre 600 All-in-One PC                                                                                                                                                            |
      | fullDescription | The A600 features a 21.5in screen, DVD or optional Blu-Ray drive, support for the full beans 1920 x 1080 HD, Dolby Home Cinema certification and an optional hybrid analogue/digital TV tuner. |
      | SKU             | LE_IC_600                                                                                                                                                                                      |
      | price           | 500                                                                                                                                                                                            |
      | tags            | computer;awesome                                                                                                                                                                               |
#      | tags            | Desktops                                                                                                                                                                                       |

    * user (check_search_result) data
      | fullDescription | Connectivity is handled by 802.11a/b/g - 802.11n is optional - and an ethernet port. You also get four USB ports, a Firewire slot, a six-in-one card reader and a 1.3- or two-megapixel webcam. |

  @RT
  @UC-2-search
  Scenario: User should be able to search for the products using GTIN from seach page
    * user go to the idpsolutions
    * user is on the page "Main"
    * user (open_search_page)
    * user is on the page "Search"
    * user (search item) "12345"
    * user is on the page "Search result"
    * user (open item) "1"
    * user is on the page "Product description"
    * user (check_search_result) data
      | productName     | Lenovo IdeaCentre 600 All-in-One PC                                                                                                                                                            |
      | fullDescription | The A600 features a 21.5in screen, DVD or optional Blu-Ray drive, support for the full beans 1920 x 1080 HD, Dolby Home Cinema certification and an optional hybrid analogue/digital TV tuner. |
      | SKU             | LE_IC_600                                                                                                                                                                                      |
      | price           | 500                                                                                                                                                                                            |
      | tags            | computer;awesome                                                                                                                                                                               |
#      | tags            | Desktops                                                                                                                                                                                       |

    * user (check_search_result) data
      | fullDescription | Connectivity is handled by 802.11a/b/g - 802.11n is optional - and an ethernet port. You also get four USB ports, a Firewire slot, a six-in-one card reader and a 1.3- or two-megapixel webcam. |

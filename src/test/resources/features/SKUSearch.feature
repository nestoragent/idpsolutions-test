Feature: Search for SKU

  @RT
  @UC-5
  Scenario: Search for SKU from main page
    * user go to the idpsolutions
    * user is on the page "Main"
    * user (search item) "AD_CS4_PH"
    * user is on the page "Search result"
    * user (open item) "1"
    * user is on the page "Product description"
    * user (check_search_result) data
      | productName      | Adobe Photoshop CS4                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
      | shortDescription | Easily find and view all your photos                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
      | fullDescription  | Adobe Photoshop CS4 software combines power and simplicity so you can make ordinary photos extraordinary; tell engaging stories in beautiful, personalized creations for print and web; and easily find and view all your photos. New Photoshop.com membership* works with Photoshop CS4 so you can protect your photos with automatic online backup and 2 GB of storage; view your photos anywhere you are; and share your photos in fun, interactive ways with invitation-only Online Albums. |
      | SKU              | AD_CS4_PH                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
      | price            | 75                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
      | tags             | computer;awesome                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |


  @RT
  @UC-5-search
  Scenario: Search for SKU from search page
    * user go to the idpsolutions
    * user is on the page "Main"
    * user (open_search_page)
    * user is on the page "Search"
    * user (search item) "AD_CS4_PH"
    * user is on the page "Search result"
    * user (open item) "1"
    * user is on the page "Product description"
    * user (check_search_result) data
      | productName      | Adobe Photoshop CS4                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
      | shortDescription | Easily find and view all your photos                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
      | fullDescription  | Adobe Photoshop CS4 software combines power and simplicity so you can make ordinary photos extraordinary; tell engaging stories in beautiful, personalized creations for print and web; and easily find and view all your photos. New Photoshop.com membership* works with Photoshop CS4 so you can protect your photos with automatic online backup and 2 GB of storage; view your photos anywhere you are; and share your photos in fun, interactive ways with invitation-only Online Albums. |
      | SKU              | AD_CS4_PH                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
      | price            | 75                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
      | tags             | computer;awesome                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
